# Generated from asciidoctor-diagram-1.5.10.gem by gem2rpm -*- rpm-spec -*-
%global gem_name asciidoctor-diagram

Name: rubygem-%{gem_name}
Version: 1.5.10
Release: 1%{?dist}
Summary: An extension for asciidoctor that adds support for UML diagram generation using PlantUML
License: MIT
URL: https://github.com/asciidoctor/asciidoctor-diagram
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
BuildRequires: %{_bindir}/rspec
BuildRequires: rubygem(rspec-expectations)
BuildRequires: rubygem(asciidoctor)
BuildRequires: %{_bindir}/dot
BuildRequires: %{_bindir}/blockdiag
BuildRequires: %{_bindir}/convert
BuildRequires: %{_bindir}/mscgen
# Java diagram generators will be added later.
# BuildRequires: java-openjdk

# Fails because it cannot access font which should available.
# BuildRequires: %{_bindir}/pdflatex

# Not packaged in Fedora.
# BuildRequires: %{_bindir}/a2s
# BuildRequires: %{_bindir}/erd
# BuildRequires: %{_bindir}/vl2vg
# BuildRequires: %{_bindir}/mermaid
# BuildRequires: %{_bindir}/shaape
# BuildRequires: %{_bindir}/svgbob
# BuildRequires: %{_bindir}/syntrax
# BuildRequires: %{_bindir}/umlet
# BuildRequires: %{_bindir}/WaveDromEditor
BuildArch: noarch

%description
Asciidoctor diagramming extension.


%package doc
Summary: Documentation for %{name}
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version}

%build
# Create the gem as gem install only works on a gem file
gem build ../%{gem_name}-%{version}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

# Remove precompiled Java .jar file
rm -f .%{gem_instdir}/lib/*.jar

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/



%check
pushd .%{gem_instdir}
for file in ./spec/{tikz,a2s,ditaa,erd,mermaid,plantuml,shaape,svgbob,syntrax,umlet,vega,wavedrom}_spec.rb
do
    mv "${file}" "${file/_spec\.rb/_spec\.rb\.disabled}"
done

sed -i "/should generate PDF files when format is set to 'pdf'/,/^  end$/ s/^/#/" spec/blockdiag_spec.rb
rspec spec
popd

%files
%dir %{gem_instdir}
%license %{gem_instdir}/LICENSE.txt
%{gem_instdir}/images
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/CHANGELOG.adoc
%doc %{gem_instdir}/README.adoc
%doc %{gem_instdir}/README_zh-CN.adoc
%{gem_instdir}/Rakefile
%{gem_instdir}/examples
%{gem_instdir}/spec

%changelog
* Sat Sep 15 2018 Jaroslav Prokop <jar.prokop@volny.cz> - 1.5.10-1
- Initial package
